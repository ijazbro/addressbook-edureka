FROM tomcat:8.5.73-jdk8-temurin-focal
RUN rm -rf /usr/local/tomcat/webapps/*
COPY target/addressbook.war /usr/local/tomcat/webapps/ROOT.war
EXPOSE 8080
CMD ["catalina.sh", "run"]